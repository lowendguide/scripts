save the custom.sh file to directory $VESTA/data/firewall/custom.sh

you can echo $VESTA to get the base directory
make it executable by chmod +x $VESTA/data/firewall/custom.sh

All done!

If you want to add more of your own rules, edit the custom.sh file and add it there.
Remember that INPUT rules are preferably entered via the VestaCP GUI.