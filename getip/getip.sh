#!/bin/sh

#Script to check Internal / External IP #
#version: 0.1                           #
#mikho@lowendguide.com                  #
#http://git.lowendguide.com             #
#Released under WTFPL                   #

if ! which curl > /dev/null; then
   echo -e "curl not found! Install? (y/n) \c"
   read REPLY
   if [ "$REPLY" = "y" ]
   then
      sudo apt-get install curl
      if ! which curl > /dev/null; then
        echo -e "curl not found, exiting"
        exit
      fi
   else
        exit
   fi
else
  echo -e "curl installed"
fi


int4=`/sbin/ifconfig $1 | grep "inet addr" | awk -Faddr: '{print $2}' | grep -v '127.0.0.1' | grep -v '127.0.0.2' | awk '{print $1}'`
int6=`/sbin/ifconfig $1 | grep "inet6 addr" | awk -Faddr: '{print $2}' | grep -v '::1/128' | awk -F/ '{print $1}'`

echo " "
echo "Your IPv4 is listed here:"
echo $int4 | tr " " "\n"
echo " "
echo "Your IPv6 is listed here:"
echo $int6 | tr " " "\n"

ext4=`/usr/bin/curl -4 -s http://wtfismyip.com/text`
ext6=`/usr/bin/curl -6 -s http://wtfismyip.com/text`

echo " "
echo "This IPv4 is used to reach the internet"
echo $ext4
echo " "
echo "This IPv6 is used to reach the internet"
echo $ext6