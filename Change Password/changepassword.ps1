[CmdletBinding( 
    SupportsShouldProcess = $True, 
    ConfirmImpact = 'low', 
    DefaultParameterSetName = '.' 
    )] 
Param (     
    [Parameter( 
     ValueFromPipeline=$True, 
     Position=0, 
     Mandatory=$True, 
     HelpMessage="Account to change password")] 
     [ValidateNotNullOrEmpty()] 
    [string]$user,
    [Parameter( 
     ValueFromPipeline=$True, 
     Position=1, 
     Mandatory=$True, 
     HelpMessage="New Password")] 
     [ValidateNotNullOrEmpty()] 
    [string]$Password,
	[Parameter( 
     ValueFromPipeline=$True, 
     Position=2, 
     Mandatory=$False, 
     HelpMessage="List of servers")] 
     [ValidateNotNullOrEmpty()] 
    [array]$computer = '.'
    ) 


$adsiuser = [adsi]"WinNT://$computer/$user,user"
$adsiuser.SetPassword($Password)
$adsiuser.SetInfo()
